class Utilities {
    static receiveDamage(name, health, damage) {
        health -= damage;
        const phrase = health >= 0 ? `${name} ha recibido ${damage} puntos de daño`
        : `${name} ha muerto en acto de combate`;
        return {
            phrase : phrase,
            health : health,
        };
    }
    static randomTwoNumbers(max, min = 0) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static attack (defender, attacker) {
        if (attacker.length && defender.length != 0) {
            const randomDefenderSoldier = Utilities.randomTwoNumbers(defender.length -1, 0);
            const randomAttackerSoldier = Utilities.randomTwoNumbers(attacker.length -1, 0);
            const damage = attacker[randomAttackerSoldier].strength;
            const valueReceiveDamage = defender[randomDefenderSoldier].receiveDamage(damage);
            if (defender[randomDefenderSoldier].health <= 0) {
                defender.splice(randomDefenderSoldier, 1);
            }
            return valueReceiveDamage;
        }
    }
}

class Jedi {
    constructor(health, strength) {
        this.health = health;
        this.strength = strength;
    }
    attack() {
        return this.strength;
    }
    receiveDamage(damage) {
        this.health -= damage;
    }
};

class Sith extends Jedi {
    constructor(name, health, strength){
        super(health, strength);
        this.name = name;
    }
    receiveDamage(damage) {
        const receiveDamageContent = Utilities.receiveDamage(this.name, this.health, damage);
        this.health = receiveDamageContent.health;
        return receiveDamageContent.phrase;
    }
    battleCry() {
        return '¡El lado oscuro manda!';
    }
};

class Imperial extends Jedi {
    constructor(health, strength) {
        super(health, strength);
    }
    receiveDamage(damage) {
        const receiveDamageContent = Utilities.receiveDamage('Un imperial', this.health, damage);
        this.health = receiveDamageContent.health;
        return receiveDamageContent.phrase;
    }
};

class War {
    constructor (sithArmy = [], imperialArmy = []) {
        this.sithArmy = sithArmy;
        this.imperialArmy = imperialArmy;
    }
    addSith(sith) {
        this.sithArmy.push(sith);
    }
    addImperial(imperial) {
        this.imperialArmy.push(imperial);
    }
    sithAttack() {
        return Utilities.attack(this.imperialArmy, this.sithArmy);
    }
    imperialAttack() {
        return Utilities.attack(this.sithArmy, this.imperialArmy);
    }
    showStatus() {
        if (this.imperialArmy.length === 0) return "Siths have won the war of the century!";
        if (this.sithArmy.length === 0) return "Imperials have fought for their lives and survived another day..";
        return "Siths and Imperials are still in the thick of battle.";
    }
};

const pacoSith = new Sith('Paco', 300, 120);
const imperial = new Imperial(60, 30);
const imperia2 = new Imperial(60, 30);
const imperia3 = new Imperial(60, 30);
const war = new War();
war.addSith(pacoSith);
war.addImperial(imperial);
war.addImperial(imperia2);
war.addImperial(imperia3);

console.log(war.sithArmy, war.imperialArmy); 
console.log(war.sithAttack()); 
console.log(war.imperialAttack()); 
console.log(war.imperialAttack()); 
console.log(war.showStatus()); 
console.log(war.sithArmy, war.imperialArmy); 